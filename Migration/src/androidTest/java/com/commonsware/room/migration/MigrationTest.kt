package com.commonsware.room.migration

import androidx.room.testing.MigrationTestHelper
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.natpryce.hamkrest.absent
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

private const val DB_NAME = "MigrationTests.db"
private const val TEST_ID = 1337
private const val TEST_TITLE = "Test Title"
private const val TEST_TEXT = "Test text"
private const val TEST_VERSION = 123

@RunWith(AndroidJUnit4::class)
class MigrationTest {
  @get:Rule
  val migrationTestHelper = MigrationTestHelper(
    InstrumentationRegistry.getInstrumentation(),
    NoteDatabase::class.java.canonicalName
  )

  @Test
  fun test1To2() {
    val initialDb = migrationTestHelper.createDatabase(DB_NAME, 1)

    initialDb.execSQL(
      "INSERT INTO notes (id, title, text, version) VALUES (?, ?, ?, ?)",
      arrayOf(TEST_ID, TEST_TITLE, TEST_TEXT, TEST_VERSION)
    )

    initialDb.query("SELECT COUNT(*) FROM notes").use {
      assertThat(it.count, equalTo(1))
      it.moveToFirst()
      assertThat(it.getInt(0), equalTo(1))
    }

    initialDb.close()

    val db = migrationTestHelper.runMigrationsAndValidate(
      DB_NAME,
      2,
      true,
      MIGRATION_1_2
    )

    db.query("SELECT id, title, text, version, andNowForSomethingCompletelyDifferent FROM notes")
      .use {
        assertThat(it.count, equalTo(1))
        it.moveToFirst()
        assertThat(it.getInt(0), equalTo(TEST_ID))
        assertThat(it.getString(1), equalTo(TEST_TITLE))
        assertThat(it.getString(2), equalTo(TEST_TEXT))
        assertThat(it.getInt(3), equalTo(TEST_VERSION))
        assertThat(it.getString(4), absent())
      }
  }
}