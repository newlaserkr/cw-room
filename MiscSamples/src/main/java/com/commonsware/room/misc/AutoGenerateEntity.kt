/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.*

@Entity(tableName = "autoGenerate")
data class AutoGenerateEntity(
  @PrimaryKey(autoGenerate = true)
  var id: Long,
  var text: String
) {
  @Dao
  abstract class Store {
    @Query("SELECT * FROM autoGenerate")
    abstract fun loadAll(): List<AutoGenerateEntity>

    @Query("SELECT * FROM autoGenerate WHERE id = :id")
    abstract fun findById(id: Int): AutoGenerateEntity

    fun insert(entity: AutoGenerateEntity): AutoGenerateEntity {
      entity.id = _insert(entity)

      return entity
    }

    @Insert
    abstract fun _insert(entity: AutoGenerateEntity): Long
  }
}
