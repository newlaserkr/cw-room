/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.*

@Entity(tableName = "apps")
data class AppEntity(
  @PrimaryKey
  val applicationId: String,
  val displayName: String,
  val shortDescription: String,
  val fullDescription: String,
  val latestVersionName: String,
  val lastUpdated: Long,
  val iconUrl: String,
  val packageUrl: String,
  val donationUrl: String
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM apps")
    fun loadAll(): List<AppEntity>

    @Query("SELECT applicationId, displayName, shortDescription, iconUrl FROM apps")
    fun loadListModels(): List<AppListModel>

    @Insert
    fun insert(entity: AppEntity)
  }
}

data class AppListModel(
  val applicationId: String,
  val displayName: String,
  val shortDescription: String,
  val iconUrl: String
)