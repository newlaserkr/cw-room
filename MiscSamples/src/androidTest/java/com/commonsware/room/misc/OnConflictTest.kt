package com.commonsware.room.misc

import android.database.sqlite.SQLiteConstraintException
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.natpryce.hamkrest.*
import com.natpryce.hamkrest.assertion.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

private const val TEST_TITLE = "The Entity Strikes Back"

@RunWith(AndroidJUnit4::class)
class OnConflictTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.uniqueIndex()

  @Test(expected = SQLiteConstraintException::class)
  fun defaultBehavior() {
    assertThat(underTest.loadAll(), isEmpty)

    val firstEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity will get inserted successfully"
    )

    underTest.insert(firstEntity)

    val secondEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity is doomed"
    )

    underTest.insert(secondEntity)
  }

  @Test(expected = SQLiteConstraintException::class)
  fun insertOrAbort() {
    assertThat(underTest.loadAll(), isEmpty)

    val firstEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity will get inserted successfully"
    )

    underTest.insert(firstEntity)

    val secondEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity is doomed"
    )

    underTest.insertOrAbort(secondEntity)
  }

  @Test
  fun insertOrIgnore() {
    assertThat(underTest.loadAll(), isEmpty)

    val firstEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity will get inserted successfully"
    )

    underTest.insert(firstEntity)

    val secondEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity is doomed"
    )

    underTest.insertOrIgnore(secondEntity)

    assertThat(
      underTest.loadAll(),
      allOf(hasSize(equalTo(1)), hasElement(firstEntity))
    )
  }

  @Test
  fun insertOrReplace() {
    assertThat(underTest.loadAll(), isEmpty)

    val firstEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity will get inserted successfully, then get replaced"
    )

    underTest.insert(firstEntity)

    val secondEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity is the replacement"
    )

    underTest.insertOrReplace(secondEntity)

    assertThat(
      underTest.loadAll(),
      allOf(hasSize(equalTo(1)), hasElement(secondEntity))
    )
  }
}