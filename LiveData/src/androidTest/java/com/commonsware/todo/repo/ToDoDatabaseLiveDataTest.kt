package com.commonsware.todo.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.jraska.livedata.test
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ToDoDatabaseLiveDataTest {
  @get:Rule
  val instantExecutorRule = InstantTaskExecutorRule()

  private val context =
    InstrumentationRegistry.getInstrumentation().targetContext
  private val db = ToDoDatabase.newTestInstance(context)
  private val underTest = db.todoStore()

  @Test
  fun basicCRUD() {
    val entities = arrayOf(
      ToDoEntity(description = "this is a test", isCompleted = true),
      ToDoEntity(description = "this is another test"),
      ToDoEntity(description = "this is... wait for it... yet another test")
    )

    underTest.save(*entities)
    underTest.all().test().awaitValue().assertValue { it.containsAll(entities.toList()) }

    underTest.delete(*entities)
    underTest.all().test().awaitValue().assertValue { it.isEmpty() }
  }
}